# FUx64
###  Sets the ```DFIntTaskSchedulerTargetFps``` flag in ```<roblox_version_path>/ClientSettings/ClientAppSettings.json```.

## Download

 - [Latest](https://fps.rblx.party)
 
UI

![UI](https://cdn.discordapp.com/attachments/988807834416332820/1120787771154497616/Palz2Xhi.png)

ingame

![UI](https://cdn.discordapp.com/attachments/988807834416332820/1120787604451901500/ulfaFv7f.png)
