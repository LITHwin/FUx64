﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FUx64
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        string userProfile;
        string versi;
        string str = "{\"DFIntTaskSchedulerTargetFps\": ";
        string clientsettings;
        public MainWindow()
        {
            InitializeComponent();
            checkrblx(); getv();
            userProfile = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile); vers.Content = versi;
            clientsettings = userProfile + @"\AppData\Local\Roblox\Versions\" + versi + @"\ClientSettings\ClientAppSettings.json";
            createfile();
        }

        void checkrblx()
        {
            Process[] processesByName = Process.GetProcessesByName("RobloxPlayerBeta");
            if (processesByName.Length == 1)
            {
                MessageBox.Show("Please close Roblox to avoid any conflicts.", "FUx64", MessageBoxButton.OK, MessageBoxImage.Stop);
                quit();
            }
        }

        void createfile()
        {
            if (Directory.Exists(userProfile + @"\AppData\Local\Roblox\Versions\" + versi))
            {
                if (File.Exists(clientsettings)) { }
                else
                {
                    Directory.CreateDirectory(userProfile + @"\AppData\Local\Roblox\Versions\" + versi + @"\ClientSettings");
                    File.Create(clientsettings);
                }
            }
            else
            {
                MessageBox.Show("Please update Roblox first.", "FUx64", MessageBoxButton.OK, MessageBoxImage.Stop);
                quit();
            }
        }

        void savetorblx()
        {
            StreamWriter file = new StreamWriter(clientsettings);
            file.Write(str + fps.Text + "}"); file.Close();
            MessageBox.Show("Fps unlocked and max set to " + fps.Text + "\nYou can close FUx64 now.", "FUx64", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        void quit()
        {Environment.Exit(1);}

        void getv()
        {
            WebClient client = new WebClient(); versi = client.DownloadString("https://setup.rbxcdn.com/version");
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {savetorblx();}

        private void exit_Click(object sender, RoutedEventArgs e)
        {quit();}

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {DragMove();}
        private void PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+"); e.Handled = regex.IsMatch(e.Text);
        }

        private void appicon_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("https://fps.Rblx.Party");
        }

        private void lithlogo_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("https://LITH.win");
        }

        private void h144_Click(object sender, RoutedEventArgs e)
        {
            StreamWriter file = new StreamWriter(clientsettings);
            file.Write(str + "144" + "}"); file.Close();
            MessageBox.Show("Fps unlocked and max set to 144\nYou can close FUx64 now.", "FUx64", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void h244_Click(object sender, RoutedEventArgs e)
        {
            StreamWriter file = new StreamWriter(clientsettings);
            file.Write(str + "244" + "}"); file.Close();
            MessageBox.Show("Fps unlocked and max set to 244\nYou can close FUx64 now.", "FUx64", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void h360_Click(object sender, RoutedEventArgs e)
        {
            StreamWriter file = new StreamWriter(clientsettings);
            file.Write(str + "360" + "}"); file.Close();
            MessageBox.Show("Fps unlocked and max set to 360\nYou can close FUx64 now.", "FUx64", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
